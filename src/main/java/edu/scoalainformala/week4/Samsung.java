package edu.scoalainformala.week4;

import java.util.ArrayList;

public class Samsung extends PhoneBase {

    private String androidVersion;

    public Samsung(String androidVersion, String colorPhone, String materialPhone, int batteryLife, String serialNumber) {
        super(colorPhone, materialPhone, batteryLife, serialNumber);
        this.androidVersion = androidVersion;
        super.setContacts(new ArrayList<>());
        super.setMessages(new ArrayList<>());
        super.setCallHistory(new ArrayList<>());

    }

    public String getAndroidVersion() {
        return androidVersion;
    }

    public void setAndroidVersion(String androidVersion) {
        this.androidVersion = androidVersion;
    }

}
