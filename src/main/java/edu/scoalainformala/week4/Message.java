package edu.scoalainformala.week4;

public class Message {

    private String phoneNumber;

    private String content;

    public Message(String phoneNumber, String content) {
        this.phoneNumber = phoneNumber;
        this.content = content;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "Messages{" +
                "phoneNumber='" + phoneNumber + '\'' +
                ", content='" + content + '\'' +
                '}';
    }
}
