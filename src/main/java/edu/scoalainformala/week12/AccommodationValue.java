package edu.scoalainformala.week12;

public class AccommodationValue {
   private Long id;
    private String type;
    private String bed_type;
    private String season;
    private double value;

    public AccommodationValue(Long id, String type, String bed_type, String season, double value) {
        this.id = id;
        this.type = type;
        this.bed_type = bed_type;
        this.season = season;
        this.value = value;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBed_type() {
        return bed_type;
    }

    public void setBed_type(String bed_type) {
        this.bed_type = bed_type;
    }

    public String getSeason() {
        return season;
    }

    public void setSeason(String season) {
        this.season = season;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }
}
