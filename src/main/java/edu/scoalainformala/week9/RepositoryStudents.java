package edu.scoalainformala.week9;

import edu.scoalainformala.week9.Exceptions.DateOfBirthInvalidException;
import edu.scoalainformala.week9.Exceptions.GenderInvalidException;
import edu.scoalainformala.week9.Exceptions.NameInvalidException;
import edu.scoalainformala.week9.Exceptions.StudentDoesntExist;
import lombok.extern.slf4j.Slf4j;

import java.time.LocalDate;
import java.time.Period;
import java.util.*;

@Slf4j
public class RepositoryStudents {

    private List<Student> students;

    public List<Student> getStudents() {
        return students;
    }


    public void setStudents(List<Student> students) {
        this.students = students;
    }


    public RepositoryStudents() {
        this.students = new ArrayList<>();
    }

    public List<Student> listStudent() {
        for (Student student : students) {
            System.out.println(student.toString());

        }

        return students;
    }

    public List<Student> orderedByLastName() {
        log.info("Order the list of students by Last Name:");
        Collections.sort(students, Comparator.comparing(Student::getLastName));

        return students;
    }

    public List<Student> orderedByDateOfBirth() {
        log.info("Order the list of students by date of birth:");
        Collections.sort(students, Comparator.comparing(Student::getDateOfBirth));

        return students;
    }

    public void addStudent(Student student) {
        log.info("Adding students: ");
        try {
            Validator.nameIsNotEmpty(student.getFirstName(), student.getLastName());
            Validator.validateDateOfBirth(student.getDateOfBirth());
            Validator.genderValidation(student.getGender());
            students.add(student);
        } catch (GenderInvalidException | NameInvalidException | DateOfBirthInvalidException e) {
            log.error("An error occurred while adding student: {}", e.getMessage());
        }
        log.info("Student {} {} added to the list", student.getFirstName(), student.getLastName());
    }

    public void
    deleteStudent(String ID) throws StudentDoesntExist {
        boolean studentExists = false;
        for (int i = students.size() - 1; i >= 0; i--) {
            Student student = students.get(i);
            if (Objects.equals(student.getID(), ID)) {
                studentExists = true;
                students.remove(i);
            }
        }
        if (!studentExists) {
            throw new StudentDoesntExist("Student doesn't exist!");
        }
    }

    public List<Student> getStudentsWithAge(int age) throws Exception {
        try {
            if (age < 0) {
                throw new IllegalArgumentException("Age should be a positive number.");
            }
            List<Student> studentsWithAge = new ArrayList<>();
            for (Student student : students) {

                int studentAge = calculateAge(student.getDateOfBirth());
                if (studentAge == age) {
                    studentsWithAge.add(student);
                }
            }
            return studentsWithAge;
        } catch (IllegalArgumentException e) {
            return Collections.emptyList();
        }
    }

    private int calculateAge(LocalDate dateOfBirth) throws Exception {
        LocalDate currentDate = LocalDate.now();
        int age = Period.between(dateOfBirth, currentDate).getYears();
        if (age < 0) {
            throw new IllegalArgumentException("Age calculation resulted in a negative value.");
        }
        return age;
    }

}



