package edu.scoalainformala.week9;

import edu.scoalainformala.week9.Exceptions.DateOfBirthInvalidException;
import edu.scoalainformala.week9.Exceptions.GenderInvalidException;
import edu.scoalainformala.week9.Exceptions.NameInvalidException;
import lombok.extern.slf4j.Slf4j;

import java.time.LocalDate;

@Slf4j
public class Validator {

    public static void validateDateOfBirth(LocalDate dateOfBirth) throws DateOfBirthInvalidException {
        log.info("Validating date of birth...");
        LocalDate currentDate = LocalDate.now();
        int currentYear = currentDate.getYear();
        int minBirthYear = currentYear - 18;
        int maxBirthYear = 1900;
        if (dateOfBirth == null || dateOfBirth.getYear() <= maxBirthYear || dateOfBirth.getYear() >= minBirthYear) {
            log.error("Date of birth is invalid. dateOfBirth: {}, maxBirthYear: {}, minBirthYear: {}", dateOfBirth, maxBirthYear, minBirthYear);
            throw new DateOfBirthInvalidException("The date of birth canno't be empty!");
        }
    }

    public static void nameIsNotEmpty(String firstName, String lastName) throws NameInvalidException {
        log.info("Checking the name...");
        if ((firstName == null || firstName.isEmpty()) || (lastName == null || lastName.isEmpty())) {
            log.error("First name or last name is empty. firstName: {}, lastName: {}", firstName, lastName);
            throw new NameInvalidException("The name canno't pe null!");
        }
    }

    public static void genderValidation(String gender) throws GenderInvalidException {
        log.info("Validating gender...");
        if (gender == null) {
            log.error("Gender can't be null. gender: {}", (Object) null);
            throw new GenderInvalidException("Gender is invalid");
        }
        String lowercaseGender = gender.toLowerCase();
        if (!(lowercaseGender.equals("male") || lowercaseGender.equals("female") ||
                lowercaseGender.equals("m") || lowercaseGender.equals("f"))) {
            throw new GenderInvalidException("Gender is invalid");
        }

    }
}
