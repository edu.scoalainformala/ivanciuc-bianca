package edu.scoalainformala.week9.Exceptions;

public class GenderInvalidException extends Exception {

    public GenderInvalidException (String message) {
        super(message);
    }
}
