package edu.scoalainformala.week9.Exceptions;

public class NameInvalidException extends Exception {

    public NameInvalidException (String message) {
        super(message);
    }
}
