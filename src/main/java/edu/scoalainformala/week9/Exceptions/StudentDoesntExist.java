package edu.scoalainformala.week9.Exceptions;

public class StudentDoesntExist extends Exception {

    public StudentDoesntExist (String message) {
        super(message);
    }
}
