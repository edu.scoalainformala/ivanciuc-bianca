package edu.scoalainformala.week9.Exceptions;

public class DateOfBirthInvalidException extends Exception {

    public DateOfBirthInvalidException (String message) {
        super(message);
    }
}


