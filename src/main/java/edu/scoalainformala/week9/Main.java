package edu.scoalainformala.week9;

import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.PropertyConfigurator;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Slf4j
public class Main {

    public static void main(String[] args) throws Exception {
        String log4jConfPath = "src/main/resources/log4j.properties";
        PropertyConfigurator.configure(log4jConfPath);

        RepositoryStudents students = new RepositoryStudents();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");


        students.addStudent(new Student(LocalDate.parse("02.10.1998", formatter), "Andreea", "Pop", "female", "1945"));
        students.addStudent(new Student(LocalDate.parse("04.10.1994", formatter), "Alex", "Molnar", "m", "4456"));
        students.addStudent(new Student(LocalDate.parse("02.10.2003", formatter), "Darius", "Grigore", "MALE", "3455"));
        students.addStudent(new Student(LocalDate.parse("04.05.2005", formatter), "Ana", "Timis", "FEMALE", "4233"));
        students.addStudent(new Student(LocalDate.parse("02.06.1990", formatter), "Bogdan", "Grigore", "male", "1864"));
        students.addStudent(new Student(LocalDate.parse("02.09.1976", formatter), "Adriana", "Marinescu", "f", "9503"));

        log.info("Deleting a student from students list: ");
        students.listStudent();
        students.deleteStudent("1945");

        System.out.println();
        System.out.println();

        log.info("Noua lista fara studentul sters precedent: ");
        students.listStudent();

        System.out.println();
        System.out.println();

        log.info("Studenti ordonati dupa numele de familie: ", students.getStudents());
        students.orderedByLastName();
        students.listStudent();

        System.out.println();
        System.out.println();

        log.info("Studenti ordonati dupa data nasterii: ", students.getStudents());
        students.orderedByDateOfBirth();
        students.listStudent();

        System.out.println();
        System.out.println();

        List<Student> studentsWithAge26 = students.getStudentsWithAge(26);
        for (Student student : studentsWithAge26) {
            log.info("Afisarea studentiilor cu o anumita varsta:", student.getFirstName() + student.getLastName() + " - " + student.getDateOfBirth() + student.getGender());
        }
    }
}


