package edu.scoalainformala.week9;

import java.time.LocalDate;

public class Student {

    private LocalDate dateOfBirth;

    private String firstName;

    private String lastName;

    private String gender;

    private String ID;


    public Student (LocalDate dateOfBirth, String firstName, String lastName, String gender, String ID) {
        this.dateOfBirth = dateOfBirth;
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
        this.ID = ID;
    }


    public LocalDate getDateOfBirth () {
        return dateOfBirth;
    }

    public void setDateOfBirth (LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getFirstName () {
        return firstName;
    }

    public void setFirstName (String firstName) {
        this.firstName = firstName;
    }

    public String getLastName () {
        return lastName;
    }

    public void setLastName (String lastName) {
        this.lastName = lastName;
    }

    public String getGender () {
        return gender;
    }

    public void setGender (String gender) {
        this.gender = gender;
    }

    public String getID () {
        return ID;
    }

    public void setID (String ID) {
        this.ID = ID;
    }

    @Override
    public String toString () {
        return "Student{" +
                "dateOfBirth='" + dateOfBirth + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", gender='" + gender + '\'' +
                ", ID='" + ID + '\'' +
                '}';
    }
}

