package edu.scoalainformala.week6;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Calculator {

    public String expresie;

    private static String unitateMasura;

    public Calculator() {
        this.expresie = expresie;
        this.unitateMasura = unitateMasura;
    }

    public static String getUnitateMasura(String expresie) {
        return unitateMasura;
    }

    public String getExpresie() {
        return expresie;
    }

    public void setExpresie(String expresie) {
        this.expresie = expresie;
    }

    public static void setUnitateMasura(String unitateMasura) {
        Calculator.unitateMasura = unitateMasura;
    }

    public String calculate() {
        List<String> listaElemente = splitStringByOperators(expresie);
        List<String> listaOperatori = extractOperators(expresie);
        List<Integer> valori = parseElements(listaElemente, this.unitateMasura);
        return (calculeazaDistanta(listaOperatori, valori) + " " + this.unitateMasura);
    }

    private List<String> splitStringByOperators(String expresie) {
        if (expresie == null) {
            return new ArrayList<>();
        }
        return Arrays.asList(expresie.split("[+-]"));
    }

    private List<String> extractOperators(String expresie) {
        if (expresie == null) {
            return new ArrayList<>();
        }
        char[] charArray = expresie.toCharArray();
        List<String> operatori = new ArrayList<>();
        for (char c : charArray) {
            if (c == '+' || c == '-') {
                operatori.add(String.valueOf(c));
            }
        }
        return operatori;

    }

    private List<Integer> parseElements(List<String> listaElemente, String unitateDeMasura) {
        List<Integer> valori = new ArrayList<>();
        for (String s : listaElemente) {
            String elementWithoutSpace = s.trim();
            List<String> elementeSeparate = Arrays.asList(elementWithoutSpace.split(" "));
            try {
                UnitateDeMasura udm = UnitateDeMasura.valueOf(unitateDeMasura);
                convertToUnitateMasura(udm, valori, elementeSeparate);
            } catch (Exception e) {
                // System.out.println("Unitatea de masura nu este suportata");
                System.out.println(e.getMessage());
                throw new RuntimeException(e.getMessage(), e.getCause());
            }
        }
        return valori;
    }

    private static void convertToUnitateMasura(UnitateDeMasura unitateDeMasura, List<Integer> valori, List<String> elementeSeparate) {
        if (unitateDeMasura.equals(UnitateDeMasura.MM)) {
            valori.add(Converter.convertToMilimeter(Integer.parseInt(elementeSeparate.get(0)), elementeSeparate.get(1)));
        } else if (unitateDeMasura.equals(UnitateDeMasura.CM)) {
            valori.add(Converter.convertToCm(Integer.parseInt(elementeSeparate.get(0)), elementeSeparate.get(1)));
        } else if (unitateDeMasura.equals(UnitateDeMasura.DM)) {
            valori.add(Converter.convertToDm(Integer.parseInt(elementeSeparate.get(0)), elementeSeparate.get(1)));
        } else if (unitateDeMasura.equals(UnitateDeMasura.M)) {
            valori.add(Converter.convertToM(Integer.parseInt(elementeSeparate.get(0)), elementeSeparate.get(1)));
        } else if (unitateDeMasura.equals(UnitateDeMasura.KM)) {
            valori.add(Converter.convertToKm(Integer.parseInt(elementeSeparate.get(0)), elementeSeparate.get(1)));
        }
    }

    public int calculeazaDistanta(List<String> operatori, List<Integer> valori) {
        if (valori.isEmpty()) {
            return 0;
        }
        int rezultat = valori.get(0);
        for (int i = 0; i < operatori.size(); i++)
            if (operatori.get(i).equals("+"))
                rezultat += valori.get(i + 1);
            else {
                rezultat -= valori.get(i + 1);
            }
        return rezultat;
    }
}
