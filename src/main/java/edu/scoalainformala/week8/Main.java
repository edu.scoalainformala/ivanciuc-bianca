package edu.scoalainformala.week8;

import java.io.IOException;
import java.util.List;
import java.util.TreeSet;


public class Main {

    public static void main (String[] args) throws IOException {

        String csvFile = "C:\\Users\\ivanc\\IdeaProjects\\ivanciuc-bianca\\src\\main\\java\\edu\\scoalainformala\\week7\\result.csv";
        CsvParser csvParser = new CsvParser();
        List<Athlet> athletList = csvParser.parseAthletCsv(csvFile);
        TreeSet<Athlet> athletesSet = new TreeSet<>();
        athletList.forEach(athlet -> {
            athlet.calculateFinalResult();
            athletesSet.add(athlet);
        });

        int position = 1;
        System.out.println("Ordinea castigatorilor este: ");
        for (

                Athlet athlet : athletesSet) {
            athlet.setPosition(Placement.fromInt(position));
            System.out.println(position + ". " + athlet);
            position++;
        }

    }

}