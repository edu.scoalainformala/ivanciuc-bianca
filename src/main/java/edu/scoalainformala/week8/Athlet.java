package edu.scoalainformala.week8;

import org.jetbrains.annotations.NotNull;

import java.time.Duration;

public class Athlet implements Comparable<Athlet> {

    private int athleteNumber;

    private String athleteName;

    private String countryCode;

    private String skiTimeResult;

    private Duration totalDuration;

    private ShootingRange shootingRange;


    private Placement position;

    public void setPosition (Placement position) {
        this.position = position;
    }

    public Athlet (int athleteNumber, String athleteName, String countryCode, String skiTimeResult, ShootingRange shootingRange) {
        this.athleteNumber = athleteNumber;
        this.athleteName = athleteName;
        this.countryCode = countryCode;
        this.skiTimeResult = skiTimeResult;
        this.shootingRange = shootingRange;
    }


    private Duration parseTime () {
        String[] parts = this.skiTimeResult.split(":");
        int minutes = Integer.parseInt(parts[0]);
        int seconds = Integer.parseInt(parts[1]);
        return Duration.ofMinutes(minutes).plusSeconds(seconds);
    }


    public void calculateFinalResult () {
        Duration duration = parseTime();
        this.totalDuration = duration.plusSeconds(shootingRange.penaltyResult());
    }

    public Duration getTotalDuration () {
        return totalDuration;
    }

    @Override
    public int compareTo (@NotNull Athlet o) {
        return this.totalDuration.compareTo(o.getTotalDuration());
    }

    @Override
    public String toString () {
        return "Athlet{" +
                "athleteNumber=" + athleteNumber +
                ", athleteName='" + athleteName + '\'' +
                ", countryCode='" + countryCode + '\'' +
                ", skiTimeResult='" + skiTimeResult + '\'' +
                ", totalDuration=" + totalDuration +
                ", shootingRange=" + shootingRange +
                ", position=" + position +
                '}';
    }

    public int getAthleteNumber () {
        return athleteNumber;
    }

    public void setAthleteNumber (int athleteNumber) {
        this.athleteNumber = athleteNumber;
    }

    public String getAthleteName () {
        return athleteName;
    }

    public void setAthleteName (String athleteName) {
        this.athleteName = athleteName;
    }

    public String getCountryCode () {
        return countryCode;
    }

    public void setCountryCode (String countryCode) {
        this.countryCode = countryCode;
    }

    public String getSkiTimeResult () {
        return skiTimeResult;
    }

    public void setSkiTimeResult (String skiTimeResult) {
        this.skiTimeResult = skiTimeResult;
    }

    public void setTotalDuration (Duration totalDuration) {
        this.totalDuration = totalDuration;
    }

    public ShootingRange getShootingRange () {
        return shootingRange;
    }

    public void setShootingRange (ShootingRange shootingRange) {
        this.shootingRange = shootingRange;
    }

    public Placement getPosition () {
        return position;
    }
}


