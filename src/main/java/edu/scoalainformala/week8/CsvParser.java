package edu.scoalainformala.week8;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CsvParser {

    public List<Athlet> parseAthletCsv(String path) throws IOException {
        List<Athlet> athlets = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(path))) {
            String line;
            while ((line = reader.readLine()) != null) {
                String[] parts = line.split(",");

                int athleteNumber = Integer.parseInt(parts[0]);
                String athleteName = parts[1];
                String countryCode = parts[2];
                String skiTimeResult = parts[3];
                String firstShooting = parts[4];
                String secondShooting = parts[5];
                String thirdShooting = parts[6];

                Athlet athlet = new Athlet(athleteNumber, athleteName, countryCode, skiTimeResult,
                        new ShootingRange(firstShooting, secondShooting, thirdShooting));
                athlets.add(athlet);
            }
            return athlets;
        }
    }

}
