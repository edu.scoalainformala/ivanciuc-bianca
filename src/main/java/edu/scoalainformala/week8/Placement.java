package edu.scoalainformala.week8;

public enum Placement {
    WINNER(1),
    RUNNER_UP(2),
    THIRD_PLACE(3);

    protected int position;
    Placement(int position) {
        this.position = position;
    }


    public static Placement fromInt(int position) {
        for (Placement placement : Placement.values()) {
            if (placement.position == position) {
                return placement;
            }
        }
        throw new IllegalArgumentException("Only 3 players are allowed! " + position);
    }
}
