package edu.scoalainformala.week8;

public class ShootingRange {

    private String firstShooting;

    private String secondShooting;

    private String thirdShooting;

    public ShootingRange (String firstShooting, String secondShooting, String thirdShooting) {
        this.firstShooting = firstShooting;
        this.secondShooting = secondShooting;
        this.thirdShooting = thirdShooting;
    }

    private int penaltyCalculate (String shooting) {
        int penalty = 0;
        char[] charArray = shooting.toCharArray();
        for (char c : charArray) {
            if (c == 'o') {
                penalty += 10;
            }
        }
        return penalty;
    }

    public int penaltyResult () {
        return penaltyCalculate(firstShooting) + penaltyCalculate(secondShooting) + penaltyCalculate(thirdShooting);
    }

}
