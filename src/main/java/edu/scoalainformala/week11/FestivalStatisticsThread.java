package edu.scoalainformala.week11;

import java.util.HashMap;
import java.util.Map;

public class FestivalStatisticsThread extends Thread {
    private final FestivalGate gate;

    public FestivalStatisticsThread(FestivalGate gate) {
        this.gate = gate;
        this.setName("Statisticiile Threadurilor:");
    }

    @Override
    public void run() {
        try {
            Map<TicketType, Integer> ticketCounts = new HashMap<>();

            while (true) {

                Thread.sleep(5000);

                ticketCounts.clear();

                while (!gate.ticketQueue.isEmpty()) {
                    TicketType ticketType = gate.ticketQueue.poll();
                    ticketCounts.put(ticketType, ticketCounts.getOrDefault(ticketType, 0) + 1);
                }

                // Print statistics
                System.out.println("Numarul de persoane intrate: " + ticketCounts.values().stream().mapToInt(Integer::intValue).sum());
                for (TicketType ticketType : TicketType.values()) {
                    int count = ticketCounts.getOrDefault(ticketType, 0);
                    System.out.println(count + " persoane, detin :  " + ticketType.toString().toLowerCase().replace("_", " ") + " ticket");
                }
                System.out.println();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
