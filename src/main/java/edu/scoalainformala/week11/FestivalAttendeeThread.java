package edu.scoalainformala.week11;

import java.util.Random;

public class FestivalAttendeeThread extends Thread{
    private final FestivalGate gate;

    public FestivalAttendeeThread(TicketType ticketType, FestivalGate gate) {
        this.gate = gate;
        this.setName(ticketType.toString() + " Participanti");
    }

    @Override
    public void run() {
        try {
            Random random = new Random();
            while (true) {
                TicketType ticketType = TicketType.values()[random.nextInt(TicketType.values().length)];
                gate.validateTicket(ticketType);
                Thread.sleep(random.nextInt(1000));
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
