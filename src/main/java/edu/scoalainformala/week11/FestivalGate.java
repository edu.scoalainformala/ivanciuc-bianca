package edu.scoalainformala.week11;

import java.util.LinkedList;
import java.util.Queue;

public class FestivalGate {

    final Queue<TicketType> ticketQueue = new LinkedList<>();

    public synchronized void validateTicket(TicketType ticketType) {
        ticketQueue.offer(ticketType);
        notify();
    }

}
