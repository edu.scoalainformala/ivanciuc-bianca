package edu.scoalainformala.week11;

public class Main {
    public static void main(String[] args) {

        FestivalGate gate = new FestivalGate();
        FestivalStatisticsThread statisticsThread = new FestivalStatisticsThread(gate);
        statisticsThread.start();

        for (int i = 0; i < 100; i++) {
            TicketType tickgetType = TicketType.values()[(int) (Math.random() * TicketType.values().length)];
            FestivalAttendeeThread attendee = new FestivalAttendeeThread(tickgetType, gate);
            attendee.start();
        }
    }


}

