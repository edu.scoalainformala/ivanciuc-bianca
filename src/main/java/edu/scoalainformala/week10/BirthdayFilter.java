package edu.scoalainformala.week10;

import java.util.List;
import java.util.stream.Collectors;

public class BirthdayFilter {
    public static List<Person> filterByMonth(List<Person> people, int targetMonth) {
        return people.stream()
                .filter(person -> person.getDateOfBirth().getMonthValue() == targetMonth)
                .sorted((p1, p2) -> p1.getLastName().compareToIgnoreCase(p2.getLastName()))
                .collect(Collectors.toList());
    }
}
