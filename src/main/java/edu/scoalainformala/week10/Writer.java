package edu.scoalainformala.week10;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class Writer {

    public static void writeToFile(List<Person> people, String filename) throws IOException {
        File outputFile = new File(filename);
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(outputFile)))  {
            for (Person person : people) {
                writer.write(person.getFirstName() + ", " + person.getLastName());
                writer.newLine();
            }
        }
    }
}
