package edu.scoalainformala.week10;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

public class Reader {
    public static List<Person> readFromFile(String  path) throws IOException {
        Path fileName = Path.of(path);
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName.toFile()))) {
            return reader.lines()
                    .map(line -> {
                        String[] parts = line.split(",");
                        String firstName = parts[0].trim();
                        String lastName = parts[1].trim();
                        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
                        LocalDate dateOfBirth = LocalDate.parse(parts[2].trim(), formatter);
                        return new Person(firstName, lastName, dateOfBirth);
                    })
                    .collect(Collectors.toList());
        }
    }
}
