package edu.scoalainformala.week10;

import java.io.IOException;
import java.util.List;

import static edu.scoalainformala.week10.BirthdayFilter.filterByMonth;
import static edu.scoalainformala.week10.Reader.readFromFile;
import static edu.scoalainformala.week10.Writer.writeToFile;

public class Main {
    public static void main(String[] args) {

        String inputFilename = args[0];
        int targetMonth = Integer.parseInt(args[1]);
        String outputFilename = args[2];
        String outputFileNameFullPath = "src/main/java/edu/scoalainformala/week9/" + outputFilename;
        try {
            List<Person> people = readFromFile(inputFilename);
            List<Person> filteredPeople = filterByMonth(people, targetMonth);
            writeToFile(filteredPeople, outputFileNameFullPath);
            System.out.println("Filtered records have been written to " + outputFilename);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
