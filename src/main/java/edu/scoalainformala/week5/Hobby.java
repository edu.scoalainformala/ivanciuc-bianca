package edu.scoalainformala.week5;

import java.util.List;

public class Hobby {

    private String nameHobby;

    private int frequency;

    private List<Address> addresses;

    public Hobby(String nameHobby, int frequency, List<Address> addresses) {
        this.nameHobby = nameHobby;
        this.frequency = frequency;
        this.addresses = addresses;
    }

    public String getNameHobby() {
        return nameHobby;
    }

    public void setNameHobby(String nameHobby) {
        this.nameHobby = nameHobby;
    }

    public int getFrequency() {
        return frequency;
    }

    public void setFrequency(int frequency) {
        this.frequency = frequency;
    }

    public List<Address> getAddresses() {
        return addresses;
    }

    public void setAddresses(List<Address> addresses) {
        this.addresses = addresses;
    }

    @Override
    public String toString() {

        return nameHobby + " every " + frequency + " times a week. And the addresses for hobby is: " + addresses;
    }
}
