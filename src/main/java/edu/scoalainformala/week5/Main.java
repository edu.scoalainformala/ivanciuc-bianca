package edu.scoalainformala.week5;

import java.util.*;

public class Main {

    public static void main(String[] args) {

        Address address1 = new Address("Sun", "Hawaii", "USA");
        Address address2 = new Address("SecondF", "New Jersey", "USA");
        Address address3 = new Address("Upside", "NewYork", "USA");

        List<Address> addresses1 = new ArrayList<>();
        List<Address> addresses2 = new ArrayList<>();
        List<Address> addresses3 = new ArrayList<>();

        addresses1.add(address1);
        addresses2.add(address2);
        addresses3.add(address3);

        Hobby hobby1 = new Hobby("fishing", 5, addresses2);
        Hobby hobby2 = new Hobby("reading", 10, addresses1);
        Hobby hobby3 = new Hobby("running", 4, addresses3);

        List<Hobby> hobbies1 = new ArrayList<>();
        List<Hobby> hobbies2 = new ArrayList<>();
        List<Hobby> hobbies3 = new ArrayList<>();
        hobbies1.add(hobby1);
        hobbies2.add(hobby2);
        hobbies3.add(hobby3);

        Person person1 = new Person("Alex", 40, hobbies2);
        Person person2 = new Person("Ioana", 20, hobbies1);
        Person person3 = new Person("Diana", 30, hobbies3);

        Set<Person> persons = new TreeSet<>();
        persons.add(person1);
        persons.add(person2);
        persons.add(person3);

        for (Person person : persons) {
            System.out.println(person);
        }

        person1.getHobbies().add(hobby3);

        Map<Person, List<Hobby>> personHobbiesMap = new HashMap<>();
        personHobbiesMap.put(person1, person1.getHobbies());
        personHobbiesMap.put(person2, person2.getHobbies());
        personHobbiesMap.put(person3, person3.getHobbies());

        System.out.println(" ");
        System.out.println(" ");
        System.out.println(" ");
        List<Hobby> selectedPersonHobbies = personHobbiesMap.get(person1);

        for (Hobby hobby : selectedPersonHobbies) {
            System.out.println(person1.getName()+ " is "+ hobby);
        }
    }
}