package edu.scoalainformala.week6;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class CalculatorTest {
    @Test
    void testCalculeazaDistanta() {
        List<String> operatori = new ArrayList<>();
        operatori.add("+");
        operatori.add("-");
        operatori.add("+");

        List<Integer> valori = new ArrayList<>();
        valori.add(10);
        valori.add(5);
        valori.add(2);
        valori.add(8);

        Calculator calculator = new Calculator();
        int rezultat = calculator.calculeazaDistanta(operatori, valori);

        assertEquals(21, rezultat);
    }
}