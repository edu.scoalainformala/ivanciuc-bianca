package edu.scoalainformala.week12;


import org.junit.jupiter.api.Test;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class TestSQL {


    @Test
    public void testInsert() {
        Connection connection = null;
        try {
            String url = "jdbc:postgresql://localhost:5432/Tema";
            connection = DriverManager.getConnection(url, "postgres", "1998");
            String insertQuery = "INSERT INTO \"Tema\".accommodation (type, bed_type, max_guests, description) VALUES (?, ?,?,?)";
            PreparedStatement preparedStatement = connection.prepareStatement(insertQuery);
            preparedStatement.setString(1, "MountainHouse");
            preparedStatement.setString(2, "King");
            preparedStatement.setInt(3, 8);
            preparedStatement.setString(4, "Beautiful view.");
            int rowsAffected = preparedStatement.executeUpdate();
            assertEquals(1, rowsAffected);
        } catch (SQLException e) {
            System.err.println("Eroare la conectare: " + e.getMessage());
        } finally {
            try {
                if (connection != null)
                    connection.close();
            } catch (SQLException e) {
                System.err.println("Eroare la închiderea conexiunii: " + e.getMessage());
            }
        }
    }


    @Test
    public void testSelectJoin() {

        try {
            String url = "jdbc:postgresql://localhost:5432/Tema";
            Connection connection;
            connection = DriverManager.getConnection(url, "postgres", "1998");
            Statement statement = null;
            ResultSet resultSet = null;
            final String format = "%-20s%-20s%-20s%-20s%-20s\n";
            List<AccommodationValue> accommodationValueList = new ArrayList<>();
            try {
                statement = connection.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
                resultSet = statement.executeQuery("SELECT accommodation.id, accommodation.type, accommodation.bed_type, room_fair.season, room_fair.value FROM \"Tema\".accommodation\n" +
                        "JOIN \"Tema\".accommodation_room_fair_relation ON accommodation_room_fair_relation.accommodation_id = accommodation.id\n" +
                        "JOIN \"Tema\".room_fair ON room_fair.id = accommodation_room_fair_relation.room_fair_id");
                boolean hasResults = resultSet.next();
                if (hasResults) {
                    System.out.format(format, "ID", "Type", "Bed Type", "Season", "Price");
                    do {
                        AccommodationValue accommodationValue = new AccommodationValue(resultSet.getLong("id"), resultSet.getString("type"),
                                resultSet.getString("bed_type"), resultSet.getString("season"), resultSet.getDouble("value"));
                        System.out.format(format, resultSet.getString("id"), resultSet.getString("type"),
                                resultSet.getString("bed_type"), resultSet.getString("season"), resultSet.getDouble("value"));
                        accommodationValueList.add(accommodationValue);
                    }
                    while (resultSet.next());

                    assertEquals(6, accommodationValueList.size());
                    assertEquals("Beach House", accommodationValueList.get(0).getType());
                    assertEquals(200, accommodationValueList.get(5).getValue());

                } else {
                    System.out.println();
                }
            } catch (SQLException e) {
                System.err.println(e.getMessage());
            } finally {
                if (resultSet != null) try {
                    resultSet.close();
                } catch (SQLException e) {
                }
                if (statement != null) try {
                    statement.close();
                } catch (SQLException e) {
                }
                try {
                    connection.close();
                } catch (SQLException e) {
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }
}



