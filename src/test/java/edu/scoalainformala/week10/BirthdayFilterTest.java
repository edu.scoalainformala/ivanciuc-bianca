package edu.scoalainformala.week10;

import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

class BirthdayFilterTest {


    @Test
    void testFilterByMonth() {

        Person person1 = new Person("John", "Doe", LocalDate.of(1990, 3, 15));
        Person person2 = new Person("Alice", "Smith", LocalDate.of(1985, 4, 20));
        Person person3 = new Person("Bob", "Johnson", LocalDate.of(1995, 4, 5));
        Person person4 = new Person("Emily", "Brown", LocalDate.of(1988, 3, 10));


        List<Person> people = Arrays.asList(person1, person2, person3, person4);


        List<Person> filtered = BirthdayFilter.filterByMonth(people, 3);

        assertEquals(2, filtered.size());
        assertEquals("Emily", filtered.get(0).getFirstName());
        assertEquals("John", filtered.get(1).getFirstName());


        filtered = BirthdayFilter.filterByMonth(people, 4).stream()
                .sorted(Comparator.comparing(Person::getFirstName))
                .collect(Collectors.toList());

        assertEquals(2, filtered.size());
        assertEquals("Alice", filtered.get(0).getFirstName());
        assertEquals("Bob", filtered.get(1).getFirstName());
    }
}
