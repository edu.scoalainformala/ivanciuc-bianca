package edu.scoalainformala.week9;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class RepositoryStudentsTest {

    private RepositoryStudents studentRepository;

    @BeforeEach
    public void setupData(){
        studentRepository = new RepositoryStudents();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        List<Student> students = new ArrayList<>();
        students.add(new Student(LocalDate.parse("02.03.1998",formatter), "Ana", "Pop","f","43587"));
        students.add(new Student(LocalDate.parse("02.03.1997",formatter), "Ana", "SChiop","f","43588"));
        students.add(new Student(LocalDate.parse("02.03.1996",formatter), "Ana", "plop","f","43589"));
        studentRepository.setStudents(students);
    }

    // @AfterEach
    // public void clearData() {
    //     studentRepository.getStudents().clear();
    // }

    @Test
    public void addStudentTest() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        studentRepository.addStudent(new Student(LocalDate.parse("02.03.1990",formatter), "Daniel", "Gica","f","12345"));
        assertEquals(4, studentRepository.getStudents().size());
        assertEquals("Daniel", studentRepository.getStudents().get(3).getFirstName());
    }

    @Test
    public void addStudentTest_NameNotValid() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        studentRepository.addStudent(new Student(LocalDate.parse("02.03.1990",formatter), null, "Gica","f","12345"));
        assertEquals(3, studentRepository.getStudents().size());
    }
}