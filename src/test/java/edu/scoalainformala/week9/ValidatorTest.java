package edu.scoalainformala.week9;

import edu.scoalainformala.week9.Exceptions.DateOfBirthInvalidException;
import edu.scoalainformala.week9.Exceptions.GenderInvalidException;
import edu.scoalainformala.week9.Exceptions.NameInvalidException;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import static org.junit.jupiter.api.Assertions.*;

class ValidatorTest {

    @Test
    void validateDateOfBirth () {
    }

    @Test
    void nameIsNotEmpty_throwsException () throws NameInvalidException {
        assertThrows(NameInvalidException.class, () -> Validator.nameIsNotEmpty(null, "Timis"));
        assertThrows(NameInvalidException.class, () -> Validator.nameIsNotEmpty("Ana", null));
        assertThrows(NameInvalidException.class, () -> Validator.nameIsNotEmpty("", "Timis"));
        assertThrows(NameInvalidException.class, () -> Validator.nameIsNotEmpty("Ana", ""));
    }

    @Test
    void genderValidation () throws GenderInvalidException {
        assertThrows(GenderInvalidException.class, () -> Validator.genderValidation(null));
        assertThrows(GenderInvalidException.class, () -> Validator.genderValidation(""));
        assertThrows(GenderInvalidException.class, () -> Validator.genderValidation("fdgfdg"));
        assertDoesNotThrow(()-> Validator.genderValidation("f"));

        assertDoesNotThrow(()-> Validator.genderValidation("female"));
        assertDoesNotThrow(()-> Validator.genderValidation("m"));
        assertDoesNotThrow(()-> Validator.genderValidation("male"));
        assertDoesNotThrow(()-> Validator.genderValidation("MALE"));
        assertDoesNotThrow(()-> Validator.genderValidation("FEMALE"));

    }

    @Test
    void dateOfBirthValidation() throws DateOfBirthInvalidException  {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        assertDoesNotThrow(()-> Validator.validateDateOfBirth(LocalDate.parse("02.10.1998", formatter)));
        assertThrows(DateOfBirthInvalidException.class,() -> Validator.validateDateOfBirth(LocalDate.parse("02.10.1800", formatter)));
        assertThrows(DateOfBirthInvalidException.class,() -> Validator.validateDateOfBirth(LocalDate.parse("02.10.2007", formatter)));
        assertThrows(DateOfBirthInvalidException.class,() -> Validator.validateDateOfBirth(LocalDate.parse("lqkdv;ldknv", formatter)));

    }
}