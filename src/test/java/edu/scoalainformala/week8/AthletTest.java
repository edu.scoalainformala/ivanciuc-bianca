package edu.scoalainformala.week8;

import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.time.temporal.ChronoUnit;

import static org.junit.jupiter.api.Assertions.*;

class AthletTest {

    @Test
    void calculateFinalResult () {
        Athlet athlet= new Athlet(31,"Bianca","02","12:33",
                new ShootingRange("xxxxx","xoxoxo","oxxxx"));
        athlet.calculateFinalResult();
        assertEquals(athlet.getTotalDuration(), Duration.of(13, ChronoUnit.MINUTES).plusSeconds(13));
    }

}