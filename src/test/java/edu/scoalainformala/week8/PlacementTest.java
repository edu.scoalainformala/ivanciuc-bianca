package edu.scoalainformala.week8;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PlacementTest {

    @Test
    public void testFromInt() {
        assertEquals(Placement.WINNER, Placement.fromInt(1));
        assertEquals(Placement.RUNNER_UP, Placement.fromInt(2));
        assertEquals(Placement.THIRD_PLACE, Placement.fromInt(3));
    }

    @Test
    public void testInvalidFromInt() {
        assertThrows(IllegalArgumentException.class, () -> {
            Placement.fromInt(5);
        });
    }
}