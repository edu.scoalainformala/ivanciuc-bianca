package edu.scoalainformala.week8;

import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class CsvParserTest {
    String csvFile1 = "C:\\Users\\ivanc\\IdeaProjects\\ivanciuc-bianca\\src\\main\\java\\edu\\scoalainformala\\week7\\test.csv";

    @Test
    void parseAthletCsv () throws IOException {
        CsvParser csvParser1 = new CsvParser();
        List<Athlet> athletList = csvParser1.parseAthletCsv(csvFile1);
        assertEquals(athletList.size(), 3);
        assertEquals(athletList.get(0).getAthleteNumber(),12);
        assertEquals(athletList.get(1).getAthleteName(),"Selena Gomez");
        assertEquals(athletList.get(2).getCountryCode(), "CZ");
    }
}