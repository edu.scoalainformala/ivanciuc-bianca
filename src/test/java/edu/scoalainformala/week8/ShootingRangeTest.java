package edu.scoalainformala.week8;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ShootingRangeTest {

    @Test
    void penaltyResult () {
        ShootingRange shootingRange = new ShootingRange("xxxxx","xoxoxo","oxxxx");
        int expectedResult = 40;
        int actualPenaltyResult = shootingRange.penaltyResult();
        assertEquals(expectedResult, actualPenaltyResult);
    }
}